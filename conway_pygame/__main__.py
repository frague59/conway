"""Main entry point for Conway's Game of Life."""
from . import start_conway

if __name__ == "__main__":
    start_conway()
